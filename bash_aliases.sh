alias grep='grep --color'
alias ls='ls --color=auto'
alias ip='ip -c'
alias vim='nvim'
#alias rank='sudo reflector --latest 200 --protocol http --protocol https --sort rate --save /etc/pacman.d/mirrorlist'
alias rank='sudo reflector --latest 200 --protocol https --sort rate --save /etc/pacman.d/mirrorlist'
alias wanip='dig +short myip.opendns.com @resolver1.opendns.com'
alias keymap='setxkbmap -layout us -variant intl'
alias sudo='sudo '

## Docker
alias docker_purge_container='docker rm $(docker ps -a -q)'
alias docker_purge_images='docker rmi $(docker images -q)'
alias docker_purge_all='docker_purge_container; docker_purge_images'

## screen layout
alias dual='~/.screenlayout/dual.sh'
alias single='~/.screenlayout/single.sh'
