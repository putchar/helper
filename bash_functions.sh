## screenshot helper, require scrot
screenshot() {
	scrot -s '%Y%m%d_%H%M%S.png' -e 'mv $f ~/Pictures/'
}

## memory helper
memory() {
    processes="$@";
    filter="${processes// /|}"

    maxmem=`free | awk '/^Mem:/{ print $2*0.9 }'`;

    ps ax -orss,comm |
    awk 'NR>1 && /'"$filter"'/{
    tot_size += $1;
    count++;
    proc_size[$2] += $1
    proc_count[$2]++
    } END {
    print "------ Memory usage ------"
    maxmem = '$maxmem'
    for (procname in proc_size) {
    avg_size = proc_size[procname]/proc_count[procname];
    printf("%s total memory: %.2f MB\n", procname, proc_size[procname]/1024);
    printf("    Average process size: %.2f MB\n", avg_size/1024);
    printf("    Number of processes: %i\n", proc_count[procname]);
    }
    printf("\nTotal processes: %i\n", count);
    printf("Total size: %.2f MB\n", tot_size/1024);
    printf("Total memory available: %.2f MB\n", maxmem/1024)
    printf("Memory not consumed: %.2f MB\n", (maxmem - tot_size)/1024)
      }';
  }

## check certificate
sslchecker() {
	openssl x509 -in $1 -text
}

## depends on pkgfile
search_binary() {
	pkgfile $1
}

search_file_from_package() {
	 yay -Ql
}


##

## ssh -L 5902:localhost:5902 -N -f -l putchar xen-ubuntu
vnc_forward() {
	ssh -L $1:localhost:$1 -N -f -l putchar $2
}

## remove unused package (requires yay on archlinux)
purge_unused() {
	yay -Rns $(yay -Qtdq)
}

## depends on apg
generate_pass() {
    apg -m$1 -M SNCL
}
